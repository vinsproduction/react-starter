export const PAGE_TITLE = 'REACT STARTER';

export const DEV_MODE = __WEBPACK__.env.development;

export const LOADER_DELAY = DEV_MODE ? 600 : 600;
