import React, { useState, useEffect } from 'react';
import { Route } from 'react-router-dom';
import { PRIVATE } from './../router/params';
import { Route as Props } from './../router/routes';

import useLoader from './../store/loader';
import useAuth from './../store/auth';
import useRouter from './../store/router';

import { PAGE_TITLE } from '../constants';

const Page: React.FunctionComponent<Props> = props => {

    const { component: Component, exact, path, name, params } = props;

    const [pageLoad, setPageLoad] = useState(false);
    const { auth } = useAuth();
    const { route, redirect } = useRouter();
    const { startLoader } = useLoader();

    useEffect(() => {

        if (name === 'notFound') {
            redirect('/404', 'route not found');
            return;
        }

        if (name === 'login' && auth) {
            redirect('/', 'you are allready logged');
            return;
        }

        if (params.access === PRIVATE && !auth) {
            redirect('/login', 'route requires authorization');
            return;
        }

        console.log(`%c Route onEnter: ${name} `, 'background: #000;color: #FFF', path, '\r\n', { name, auth, route });

        document.title = `${PAGE_TITLE}${(PAGE_TITLE && ':')}${params.title}`;

        (async () => {

            await startLoader(params.loader.text);
            setPageLoad(true);

        })();


        return () => {

            console.log(` Route onExit: ${name} `);
        };

    }, [auth]);

    if (!pageLoad) {
        return null;
    }

    return (
        <Route
            exact={exact}
            path={path}
            component={Component}
        />
    );

};


export default Page;
