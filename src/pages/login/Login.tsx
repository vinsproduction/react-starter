import React, { useState, useEffect } from 'react';
import { toast } from 'react-toastify';

import Page from './../../components/layout/page/Page';
import Container from './../../components/layout/container/Container';
import Center from './../../components/layout/center/Center';
import Submit from './../../components/form/submit/Submit';
import Field from './../../components/form/field/Field';
import Input from './../../components/form/input/Input';

import useAuth from './../../store/auth';
import useLoader from './../../store/loader';

import './login.styl';

import { block } from 'bem-cn';

const b = block('page-login');

const Login: React.FunctionComponent = () => {

    const { login, getAuth } = useAuth();
    const { startLoader, stopLoader } = useLoader();

    const [pageLoad, setPageLoad] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {

        setPageLoad(false);
        clearForm();

        (async () => {

            await stopLoader();
            setPageLoad(true);

        })();

    }, []);

    const submit = async e => {
        e.preventDefault();

        if (email &&  password) {

            await startLoader('Авторизация');

            let err;

            [err] = await login(email, password);
            if (err) { return error(err); }

            [err] = await getAuth();
            if (err) { return error(err); }
        }
    }

    const error = err => {
        console.error(err);
        stopLoader();
        clearForm();
        toast.error('Авторизация не удалась. Попробуйте еще раз');
        return;
    }

    const clearForm = () => {
        setEmail('');
        setPassword('');
    }

    if (!pageLoad) {
        return null;
    }

    return (

        <Page center className={b()}>
            <Container>
                <Center>
                    <form className={b('form')} onSubmit={submit}>
                        <Field label="E-mail">
                            <Input name="username" value={email} onChange={value => setEmail(value)} />
                        </Field>
                        <Field label="Пароль">
                            <Input name="password" value={password} onChange={value => setPassword(value)} />
                        </Field>
                        <div className={b('form__submit')}>
                            <Submit>Войти</Submit>
                        </div>
                    </form>
                </Center>
            </Container>
        </Page>

    );

};

export default Login;
