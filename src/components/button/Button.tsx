import React from 'react';
import './button.styl';
import { redirect } from './../../store/router/actions';

import { block } from 'bem-cn';
const b = block('button');

interface Props {
    color?: 'none' | 'white';
    href: string;
    style: object;
    className: string;
    children: any;
    onClick?: () => any;
}

export default class Button extends React.Component<Props> {

    static defaultProps = {
        href: '#',
        color: 'gray',
        style: {},
        className: '',
    }

    render() {

        const { children, className, style, color, href, onClick } = this.props;

        const classes = {
            color
        }

        const props = {
            className: b(classes).mix(className),
            style,
            onClick: () => { onClick ? onClick() : redirect(href) }
        }

        return (
            <button {...props}>{children}</button>
        );
    }
}
