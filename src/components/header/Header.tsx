import React from 'react';

import Container from './../layout/container/Container';

import useAuth from './../../store/auth';
import useRouter from './../../store/router';

import './header.styl';

import { block } from 'bem-cn';
const b = block('header');

const Header: React.FunctionComponent = () => {

    const { auth, logout } = useAuth();
    const { route } = useRouter();

    return (
        <div className={b({wide: route.params.wideScreen})}>
            <Container className={b('inner')}>
                <div className={b('logo')}>
                    Logo
                </div>
                {auth &&
                    <div className={b('user')}>
                        <p>{auth.email}</p>
                        &nbsp;|&nbsp;
                        <p className={b('logout')} onClick={() => logout()}>Выход</p>
                    </div>
                }
            </Container>
        </div>

    );

};

export default Header;
