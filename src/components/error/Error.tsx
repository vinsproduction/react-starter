import React from 'react'
import Container from './../layout/container/Container';

const Error: React.FunctionComponent = ({children}) => {

    const error = children || (<span>Что-то сломалось :(</span>);

    return (
        <Container center>
            {error}
        </Container>
    )
}

export default Error;
