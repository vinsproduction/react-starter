import React from 'react';

import './page.styl';

import { block } from 'bem-cn';
const b = block('layout__page');

const Page: React.FunctionComponent<{ className?: string; center?: boolean }> = ({ className = '', center, children }) => {

    return (
        <div className={b({ center }).mix(className)}>
            {children}
        </div>
    );
}

export default Page;
