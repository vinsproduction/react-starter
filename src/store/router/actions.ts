import { dispatch } from './../store';
import { push } from 'connected-react-router';

export const redirect =  (path: string, message = ''): void => {
    console.log(`%c redirect to ${path} `, 'background: #07b12f;color: #FFF', message);
    dispatch(push(path));
    return;
};
