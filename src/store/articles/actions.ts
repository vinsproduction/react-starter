import axios from 'axios';
import { dispatch } from './../store';
import { apiErrorHandler } from './../common';
import { shuffle } from './../../common';

import * as actionNames from './../actionNames';

import { Articles } from './state';

export const getArticles = async (): Promise<[any,Articles[]]> => {

    let articles: Articles[], err: any;

    try {

        const data = await axios.get('/api/articles').then(res => res.data);

        articles = shuffle(data);

        dispatch({ type: actionNames.ARTICLES_GET, payload: articles });

    } catch (error) {

        err = apiErrorHandler(error);
    }

    return [err, articles];

};


export const addVote = async (articleId: number, vote: '1' | '-1' | '0'): Promise<[any, any]> => {

    let data, err: any;

    try {

        await axios.post(`/api/articles/${articleId}/add_vote`, {vote}).then(res => res.data);

        dispatch({ type: actionNames.ARTICLES_ADD_VOTE, payload: {id: articleId, vote} });

    } catch (error) {

        err = apiErrorHandler(error);
    }

    return [err, data];

};

