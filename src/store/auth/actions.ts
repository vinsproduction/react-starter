import axios from 'axios';
import { dispatch } from './../store';

import * as actionNames from './../actionNames';

import { Auth } from './state';

export const login = async (email: string, accessCode: string): Promise<[any, any]> => {

    let data: any, err: any;

    try {

        data = await axios.post('/api/login', { email, accessCode }).then(res => res.data);
        dispatch({ type: actionNames.USER_LOGGED_IN });

    } catch (error) {
        err = error.response || error;
    }

    return [err, data];

};

export const logout = async (): Promise<[any, any]> => {

    let data: any, err: any;

    try {

        data = await axios.post('/api/logout', {}).then(res => res.data);

        dispatch({ type: actionNames.USER_LOGGED_OUT });
        dispatch({ type: actionNames.USER_NOT_AUTH });

    } catch (error) {
        err = error.response || error;
    }

    return [err, data];

};


export const getAuth = async (): Promise<[any, Auth]> => {

    let user: Auth, err: any;

    try {

        const user = await axios.get('/api/auth').then(res => res.data);
        dispatch({ type: actionNames.USER_IS_AUTH, payload: user });

    } catch (error) {

        err = error.response || err;

        dispatch({ type: actionNames.USER_NOT_AUTH });

        // Не считаем кэтч ошибкой - пользователь не авторизован
        if (error.response && [401, 405].includes(error.response.status)) {
            err = null;
        } else {
            console.error(err);
        }

    }

    return [err, user];

};
