/** Хук с диспатчем
 *
 * Use in store/index:
 * import * as actions from './actions';
 * export default useStore<typeof state, typeof actions>('example', actions);
 *
 * in component:
 * import useExample from 'store/index'
 * const { state, action } = useExample();
 *
 * in actions
 * export const exampleAction = function () {
 *      const { getState, dispatch } = this;
 *      const state = getState();
 *      dispatch({ type: DOIT });
 * }
 *
*/

/* eslint-disable react-hooks/rules-of-hooks*/

import { useSelector } from 'react-redux';

export default function useStore<State, Actions>(storeName: string, actions?: Actions) {

    return function getStore() {

        const state = useSelector(state => state[storeName]) as State;

        const actionsList = {} as any as Actions;

        if (actions) {
            Object.keys(actions).forEach(actionName => {
                actionsList[actionName] = actions[actionName];
            });
        }


        return {[storeName]: state, ...actionsList};
    };
}
