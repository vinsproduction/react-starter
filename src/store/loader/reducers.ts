import initialState from './state';
import { actionLog } from '../common';

import * as actionNames from './../actionNames';

const Reducers = (state = initialState, action) => {

    let newState, defaultAction = false;
    const type = action.type;
    const payload = action.payload;

    switch (type) {

        case actionNames.LOADER_START:
            newState = { ...state, active: true, time: new Date().getTime(), text: payload };
            break;
        case actionNames.LOADER_STOP:
            newState = { ...state, active: false, time: 0 };
            break;
        default:
            newState = state;
            defaultAction = true;
            break;
    }

    if (!defaultAction) {
        actionLog(type, action, state, newState);
    }
    return newState;
};

export default Reducers;
