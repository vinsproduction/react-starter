import * as storeNames from './../storeNames';
import useStore from './../useStore';
import state from './state';
import * as actions from './actions';

const storeName = storeNames.LOADER;

export {
    state,
    actions,
};

export default useStore<typeof state, typeof actions>(storeName, actions);
