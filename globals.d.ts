// allow any elements
declare namespace JSX {
    interface IntrinsicElements {
        [elemName: string]: any;
    }
}

declare module '*.json' {
    const value: any;
    export default value;
}


declare var __WEBPACK__: any;
